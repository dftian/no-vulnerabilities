import json

# This is the output of the scanner that will be parsed by GitLab.
report = {
  "version": "2.3",
  "vulnerabilities": [],
  "remediations": [],
  "scan": {
    "scanner": {
      "id": "starboard",
      "name": "Starboard",
      "url": "https://github.com/aquasecurity/starboard",
      "vendor": {
        "name": "GitLab"
      },
      "version": "0.10.0"
    },
    "type": "cluster_image_scanning",
    "status": "success"
  }
}

# Print a JSON string of the report above
print(json.dumps(report))
